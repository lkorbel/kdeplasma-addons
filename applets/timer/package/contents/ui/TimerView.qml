/*
 *   SPDX-FileCopyrightText: 2008, 2014 Davide Bettio <davide.bettio@kdemail.net>
 *   SPDX-FileCopyrightText: 2015 Bernhard Friedreich <friesoft@gmail.com>
 *
 *   SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.1
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents
import org.kde.kquickcontrolsaddons 2.0 as QtExtra

import org.kde.plasma.private.timer 0.1 as TimerPlasmoid

ColumnLayout {
    id: main
    readonly property int secondsForAlert: 60

    PlasmaComponents.Label {
        id: titleLabel
        text: root.title
        visible: root.showTitle;
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 0.125 * main.height
        Layout.alignment: Qt.AlignHCenter
    }

    TimerEdit {
        id: timerDigits
        value: root.seconds
        editable: !root.running
        alertMode: root.running && (root.seconds < main.secondsForAlert)
        onDigitModified: root.seconds += valueDelta

        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignHCenter

        SequentialAnimation on opacity {
            running: root.suspended;
            loops: Animation.Infinite;
            NumberAnimation {
                duration: PlasmaCore.Units.veryLongDuration * 2;
                from: 1.0;
                to: 0.2;
                easing.type: Easing.InOutQuad;
            }
            PauseAnimation {
                duration: PlasmaCore.Units.veryLongDuration;
            }
            NumberAnimation {
                duration: PlasmaCore.Units.veryLongDuration * 2;
                from: 0.2;
                to: 1.0;
                easing.type: Easing.InOutQuad;
            }
            PauseAnimation {
                duration: PlasmaCore.Units.veryLongDuration;
            }
        }
    }

    RowLayout {
        visible: plasmoid.configuration.showButtons
        Layout.alignment: Qt.AlignHCenter

        PlasmaComponents.ToolButton {
            icon.name: "media-playback-start"
            visible: !root.running
            onClicked: root.startTimer()
            Layout.preferredHeight: 0.25 * main.height
            Layout.preferredWidth: Layout.preferredHeight
        }
        PlasmaComponents.ToolButton {
            icon.name: "media-playback-pause"
            visible: root.running
            onClicked: root.stopTimer()
            Layout.preferredHeight: 0.25 * main.height
            Layout.preferredWidth: Layout.preferredHeight
        }
        PlasmaComponents.ToolButton {
            icon.name: "media-playback-stop"
            onClicked: root.resetTimer()
            Layout.preferredHeight: 0.25 * main.height
            Layout.preferredWidth: Layout.preferredHeight
        }
        PlasmaComponents.ToolButton {
            icon.name: "chronometer"
            onClicked: menu.popup(Qt.point(x,y-menu.height))
            Layout.preferredHeight: 0.25 * main.height
            Layout.preferredWidth: Layout.preferredHeight
            PlasmaComponents.Menu {
                id: menu
                Repeater {
                    model: plasmoid.configuration.predefinedTimers
                    PlasmaComponents.MenuItem {
                        text: TimerPlasmoid.Timer.secondsToString(modelData, "hh:mm:ss")
                        PlasmaCore.ColorScope.colorGroup: PlasmaCore.Theme.NormalColorGroup
                        PlasmaCore.ColorScope.inherit: false
                        onTriggered: {
                            root.seconds = modelData
                            root.startTimer()
                        }
                    }
                }
            }
        }
    }

    function resetOpacity() {
        timerDigits.opacity = 1.0;
    }

    Component.onCompleted: {
        root.opacityNeedsReset.connect(resetOpacity);
    }
}

