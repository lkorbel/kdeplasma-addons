import QtQuick 2.5
import org.kde.plasma.core 2.0 as PlasmaCore

/*
    Simple button using graphics of svg elements

    Display graphic of specified element from svg file. If file
    contain elements with suffixes "-pressed" and "-hovered" they will
    be used respectively as button states.
 */
MouseArea {
    id: button
    property var svgFile
    property string iconName

    hoverEnabled: true
    implicitWidth: normal.naturalSize.width
    implicitHeight: normal.naturalSize.width

    PlasmaCore.SvgItem {
        id: normal
        anchors.fill: parent
        svg: svgFile
        elementId: iconName

    }
    PlasmaCore.SvgItem {
        id: hovered
        anchors.fill: parent
        svg: svgFile
        elementId: iconName + "-hovered"
        visible: button.containsMouse

    }
    PlasmaCore.SvgItem {
        id: pressed
        anchors.fill: parent
        svg: svgFile
        elementId: iconName + "-pressed"
        visible: button.pressed
    }
}
